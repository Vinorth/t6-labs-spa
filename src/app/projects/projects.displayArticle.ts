export class ArticleInfo {
  title: string;
  byline: string;
  image: string;
  description: string;
  id: number;
}
export class Article {
  articles: ArticleInfo[];
  constructor() {
    this.articles = [{
      title: 'Website Development - Published Website',
      byline: 'Written by: Max, Md. Abdul, Raj, Vinorth On: 9/19/2018',
      image: '../assets/img/html_logo.png',
      description: 'Huzzah! if you are seeing this it means that the website is up and running. Better go catch it.',
      id: 1
    }];
  }
  getArticle() {
  return this.articles;
  }
}