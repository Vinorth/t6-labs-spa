import { Component } from '@angular/core';
import { ArticleInfo, Article } from './projects.displayArticle';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent {

  articles: ArticleInfo[];

  constructor() {
    const article = new Article();
    this.articles = article.getArticle();
  }

}
