import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PostInfo, Post } from './blog-posts.displayPosts';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {

  id: number;
  posts: PostInfo[];
  displayPost: PostInfo;

  constructor(private route: ActivatedRoute) {}
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      const post = new Post();
      this.posts =  post.getPost();
      for (const postInfo of this.posts)  {
      if (this.id === postInfo.id) {
        this.displayPost = postInfo;
      }
      }
  });
  }
}
