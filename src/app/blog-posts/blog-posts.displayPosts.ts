export class PostInfo {
  title: string;
  byline: string;
  mainimage: string;
  maincaption: string;
  description: string;
  id: number;
}
export class Post {
  posts: PostInfo[];
  constructor() {
    //I may just convert this into a json file as it may get a little bit messy.
    this.posts = [{
      title: 'A Published Website!',
      byline: 'Written by: Max, Md. Abdul, Raj, Vinorth On: 9/19/2018',
      mainimage: '../assets/img/img_city.jpg',
      maincaption: 'A unrelated image of a city for some reason.',
      description: 'This is a short and sweet message. After a few months of work the website is finally up! Congratulations team,' +
      ' I hope to see great things from this team! Thanks most of all to Vinorth for the majority of the website work. - Max Belleville',
      id: 1
    }];
  }
  getPost() {
  return this.posts;
  }
}